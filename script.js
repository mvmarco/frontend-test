











//create select inputs
let from = document.querySelector('#from')
let to = document.querySelector('#to');

let cryptoData = []


fetch('https://api.exchangeratesapi.io/latest')
  .then(res => res.json())
  .then(data => {
    let options = ''
    for (let rate in data.rates) {
      options += `
<option value='${rate}_fiat'>${rate}</option>
`
    }

    from.innerHTML += options
    to.innerHTML += options
    fetch('https://api.binance.com/api/v3/ticker/price')
      .then(res => res.json())
      .then(data => {
        let crypto = data.filter(item => item.symbol.includes('BTC')).map(item => item.symbol.split('BTC')[0]).filter(item => item)
        console.log(crypto)
        cryptoData = data.filter(item => item.symbol.includes('BTC'))

        let options = ''
        crypto.forEach(symbol => {
          options += `
<option value='${symbol}_crypto'>${symbol}</option>
`
        })

        from.innerHTML += options
        to.innerHTML += options
      })
  })

//handle convert

let convert = document.querySelector('#convert')
let amount = document.querySelector('#amount')
let result = document.querySelector('#result')
let symbol = document.querySelector('#symbol')

convert.onclick = function () {
  let [toValue, toType] = to.value.split('_')
  let [fromValue, fromType] = from.value.split('_')
  if (toType == 'fiat' && fromType == 'fiat') {
    fetch(`https://api.exchangeratesapi.io/latest?base=${fromValue}&symbols=${toValue}`)
      .then(res => res.json())
      .then(data => {
        symbol.value = toValue
        result.innerText = amount.value * data.rates[toValue]
      })
  } else if (toType == 'crypto' && fromType == 'crypto') {
    let toPrice = 0
    let fromPrice = 0
    console.log(toPrice, fromPrice)
    if (toValue === 'BTC') {
      toPrice = 1
    } else {
      toPrice = cryptoData.find(item => item.symbol === toValue + 'BTC').price
    }
    if (fromValue === 'BTC') {
      fromPrice = 1
    } else {
      fromPrice = cryptoData.find(item => item.symbol === fromValue + 'BTC').price
    }
    symbol.value = toValue
    result.innerText = amount.value * fromPrice / toPrice
  } else if (toType == 'crypto' && fromType == 'fiat') {
    fetch(`https://api.exchangeratesapi.io/latest?base=${fromValue}&symbols=USD`)
      .then(res => res.json())
      .then(data => {
        symbol.value = toValue
        result.innerText = amount.value * data.rates.USD / cryptoData.find(item => item.symbol === 'BTCUSDT').price
      })

  } else {
    alert(`stuck here`)
  }
}
